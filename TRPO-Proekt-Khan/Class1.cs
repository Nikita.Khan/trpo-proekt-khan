﻿using System;
namespace TRPO_Proekt_Khan
{
    public class VkladCalculator
    {
        public double GetPercents(int K, int P, int d, int D, int N)
        {
            double S = K * (Math.Pow(1 + P / 100.0 * ((double)d / D), N) - 1);
            return S;
        }
    }
}
