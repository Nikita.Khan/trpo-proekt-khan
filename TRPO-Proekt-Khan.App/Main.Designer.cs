﻿
namespace TRPO_Proekt_Khan.App
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.MainTabControl = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.N_Text_Box = new MetroFramework.Controls.MetroTextBox();
            this.Snyatie_TextBox = new MetroFramework.Controls.MetroTextBox();
            this.Snyatie_Name = new MetroFramework.Controls.MetroLabel();
            this.Snyatie_DateBox = new MetroFramework.Controls.MetroDateTime();
            this.Snyatie_CheckBox = new MetroFramework.Controls.MetroCheckBox();
            this.Popolnenie_TextBox = new MetroFramework.Controls.MetroTextBox();
            this.Popolnenie_Name = new MetroFramework.Controls.MetroLabel();
            this.Popolnenie_DateBox = new MetroFramework.Controls.MetroDateTime();
            this.Popolnenie_CheckBox = new MetroFramework.Controls.MetroCheckBox();
            this.Start_Vklad = new MetroFramework.Controls.MetroLabel();
            this.Start_Vklad_Date_Box = new MetroFramework.Controls.MetroDateTime();
            this.TotalPanel = new MetroFramework.Controls.MetroPanel();
            this.TotalLabel = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.GetResultButton = new MetroFramework.Controls.MetroButton();
            this.TotalGrid = new MetroFramework.Controls.MetroGrid();
            this.Date_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Percent_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deposit_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d_Combo_Box = new MetroFramework.Controls.MetroComboBox();
            this.N_Name = new MetroFramework.Controls.MetroLabel();
            this.d_Name = new MetroFramework.Controls.MetroLabel();
            this.P_Name = new MetroFramework.Controls.MetroLabel();
            this.P_Text_Box = new MetroFramework.Controls.MetroTextBox();
            this.K_Name = new MetroFramework.Controls.MetroLabel();
            this.K_Text_Box = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.TotalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).BeginInit();
            this.metroTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.MainTabControl);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(915, 550);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.metroTabPage1);
            this.MainTabControl.Controls.Add(this.metroTabPage3);
            this.MainTabControl.Location = new System.Drawing.Point(0, 0);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 1;
            this.MainTabControl.Size = new System.Drawing.Size(915, 550);
            this.MainTabControl.TabIndex = 2;
            this.MainTabControl.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.N_Text_Box);
            this.metroTabPage1.Controls.Add(this.Snyatie_TextBox);
            this.metroTabPage1.Controls.Add(this.Snyatie_Name);
            this.metroTabPage1.Controls.Add(this.Snyatie_DateBox);
            this.metroTabPage1.Controls.Add(this.Snyatie_CheckBox);
            this.metroTabPage1.Controls.Add(this.Popolnenie_TextBox);
            this.metroTabPage1.Controls.Add(this.Popolnenie_Name);
            this.metroTabPage1.Controls.Add(this.Popolnenie_DateBox);
            this.metroTabPage1.Controls.Add(this.Popolnenie_CheckBox);
            this.metroTabPage1.Controls.Add(this.Start_Vklad);
            this.metroTabPage1.Controls.Add(this.Start_Vklad_Date_Box);
            this.metroTabPage1.Controls.Add(this.TotalPanel);
            this.metroTabPage1.Controls.Add(this.GetResultButton);
            this.metroTabPage1.Controls.Add(this.TotalGrid);
            this.metroTabPage1.Controls.Add(this.d_Combo_Box);
            this.metroTabPage1.Controls.Add(this.N_Name);
            this.metroTabPage1.Controls.Add(this.d_Name);
            this.metroTabPage1.Controls.Add(this.P_Name);
            this.metroTabPage1.Controls.Add(this.P_Text_Box);
            this.metroTabPage1.Controls.Add(this.K_Name);
            this.metroTabPage1.Controls.Add(this.K_Text_Box);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(907, 508);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Главная";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // N_Text_Box
            // 
            // 
            // 
            // 
            this.N_Text_Box.CustomButton.Image = null;
            this.N_Text_Box.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.N_Text_Box.CustomButton.Name = "";
            this.N_Text_Box.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.N_Text_Box.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.N_Text_Box.CustomButton.TabIndex = 1;
            this.N_Text_Box.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.N_Text_Box.CustomButton.UseSelectable = true;
            this.N_Text_Box.CustomButton.Visible = false;
            this.N_Text_Box.Lines = new string[0];
            this.N_Text_Box.Location = new System.Drawing.Point(182, 147);
            this.N_Text_Box.MaxLength = 32767;
            this.N_Text_Box.Name = "N_Text_Box";
            this.N_Text_Box.PasswordChar = '\0';
            this.N_Text_Box.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.N_Text_Box.SelectedText = "";
            this.N_Text_Box.SelectionLength = 0;
            this.N_Text_Box.SelectionStart = 0;
            this.N_Text_Box.ShortcutsEnabled = true;
            this.N_Text_Box.Size = new System.Drawing.Size(146, 23);
            this.N_Text_Box.TabIndex = 27;
            this.N_Text_Box.TabStop = false;
            this.N_Text_Box.UseSelectable = true;
            this.N_Text_Box.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.N_Text_Box.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.N_Text_Box.TextChanged += new System.EventHandler(this.N_Text_Box_TextChanged);
            this.N_Text_Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.N_Text_Box_KeyPress);
            this.N_Text_Box.Validating += new System.ComponentModel.CancelEventHandler(this.N_Text_Box_Validating);
            // 
            // Snyatie_TextBox
            // 
            // 
            // 
            // 
            this.Snyatie_TextBox.CustomButton.Image = null;
            this.Snyatie_TextBox.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.Snyatie_TextBox.CustomButton.Name = "";
            this.Snyatie_TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Snyatie_TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Snyatie_TextBox.CustomButton.TabIndex = 1;
            this.Snyatie_TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Snyatie_TextBox.CustomButton.UseSelectable = true;
            this.Snyatie_TextBox.CustomButton.Visible = false;
            this.Snyatie_TextBox.Lines = new string[0];
            this.Snyatie_TextBox.Location = new System.Drawing.Point(23, 336);
            this.Snyatie_TextBox.MaxLength = 32767;
            this.Snyatie_TextBox.Name = "Snyatie_TextBox";
            this.Snyatie_TextBox.PasswordChar = '\0';
            this.Snyatie_TextBox.PromptText = "На сумму";
            this.Snyatie_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Snyatie_TextBox.SelectedText = "";
            this.Snyatie_TextBox.SelectionLength = 0;
            this.Snyatie_TextBox.SelectionStart = 0;
            this.Snyatie_TextBox.ShortcutsEnabled = true;
            this.Snyatie_TextBox.Size = new System.Drawing.Size(146, 23);
            this.Snyatie_TextBox.TabIndex = 26;
            this.Snyatie_TextBox.UseSelectable = true;
            this.Snyatie_TextBox.WaterMark = "На сумму";
            this.Snyatie_TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Snyatie_TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.Snyatie_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Snyatie_TextBox_KeyPress);
            this.Snyatie_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Snyatie_TextBox_Validating);
            // 
            // Snyatie_Name
            // 
            this.Snyatie_Name.AutoSize = true;
            this.Snyatie_Name.Location = new System.Drawing.Point(23, 304);
            this.Snyatie_Name.Name = "Snyatie_Name";
            this.Snyatie_Name.Size = new System.Drawing.Size(53, 19);
            this.Snyatie_Name.TabIndex = 25;
            this.Snyatie_Name.Text = "Снятие";
            // 
            // Snyatie_DateBox
            // 
            this.Snyatie_DateBox.Location = new System.Drawing.Point(116, 301);
            this.Snyatie_DateBox.MinimumSize = new System.Drawing.Size(0, 29);
            this.Snyatie_DateBox.Name = "Snyatie_DateBox";
            this.Snyatie_DateBox.Size = new System.Drawing.Size(146, 29);
            this.Snyatie_DateBox.TabIndex = 24;
            // 
            // Snyatie_CheckBox
            // 
            this.Snyatie_CheckBox.AutoSize = true;
            this.Snyatie_CheckBox.Location = new System.Drawing.Point(282, 308);
            this.Snyatie_CheckBox.Name = "Snyatie_CheckBox";
            this.Snyatie_CheckBox.Size = new System.Drawing.Size(46, 15);
            this.Snyatie_CheckBox.TabIndex = 23;
            this.Snyatie_CheckBox.Text = "Есть";
            this.Snyatie_CheckBox.UseSelectable = true;
            this.Snyatie_CheckBox.CheckedChanged += new System.EventHandler(this.Snyatie_CheckBox_CheckedChanged);
            // 
            // Popolnenie_TextBox
            // 
            // 
            // 
            // 
            this.Popolnenie_TextBox.CustomButton.Image = null;
            this.Popolnenie_TextBox.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.Popolnenie_TextBox.CustomButton.Name = "";
            this.Popolnenie_TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Popolnenie_TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Popolnenie_TextBox.CustomButton.TabIndex = 1;
            this.Popolnenie_TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Popolnenie_TextBox.CustomButton.UseSelectable = true;
            this.Popolnenie_TextBox.CustomButton.Visible = false;
            this.Popolnenie_TextBox.Lines = new string[0];
            this.Popolnenie_TextBox.Location = new System.Drawing.Point(23, 263);
            this.Popolnenie_TextBox.MaxLength = 32767;
            this.Popolnenie_TextBox.Name = "Popolnenie_TextBox";
            this.Popolnenie_TextBox.PasswordChar = '\0';
            this.Popolnenie_TextBox.PromptText = "На сумму";
            this.Popolnenie_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Popolnenie_TextBox.SelectedText = "";
            this.Popolnenie_TextBox.SelectionLength = 0;
            this.Popolnenie_TextBox.SelectionStart = 0;
            this.Popolnenie_TextBox.ShortcutsEnabled = true;
            this.Popolnenie_TextBox.Size = new System.Drawing.Size(146, 23);
            this.Popolnenie_TextBox.TabIndex = 22;
            this.Popolnenie_TextBox.UseSelectable = true;
            this.Popolnenie_TextBox.WaterMark = "На сумму";
            this.Popolnenie_TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Popolnenie_TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.Popolnenie_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Popolnenie_TextBox_KeyPress);
            // 
            // Popolnenie_Name
            // 
            this.Popolnenie_Name.AutoSize = true;
            this.Popolnenie_Name.Location = new System.Drawing.Point(23, 231);
            this.Popolnenie_Name.Name = "Popolnenie_Name";
            this.Popolnenie_Name.Size = new System.Drawing.Size(87, 19);
            this.Popolnenie_Name.TabIndex = 21;
            this.Popolnenie_Name.Text = "Пополнение";
            // 
            // Popolnenie_DateBox
            // 
            this.Popolnenie_DateBox.Location = new System.Drawing.Point(116, 228);
            this.Popolnenie_DateBox.MinimumSize = new System.Drawing.Size(0, 29);
            this.Popolnenie_DateBox.Name = "Popolnenie_DateBox";
            this.Popolnenie_DateBox.Size = new System.Drawing.Size(146, 29);
            this.Popolnenie_DateBox.TabIndex = 20;
            // 
            // Popolnenie_CheckBox
            // 
            this.Popolnenie_CheckBox.AutoSize = true;
            this.Popolnenie_CheckBox.Location = new System.Drawing.Point(282, 235);
            this.Popolnenie_CheckBox.Name = "Popolnenie_CheckBox";
            this.Popolnenie_CheckBox.Size = new System.Drawing.Size(46, 15);
            this.Popolnenie_CheckBox.TabIndex = 19;
            this.Popolnenie_CheckBox.Text = "Есть";
            this.Popolnenie_CheckBox.UseSelectable = true;
            this.Popolnenie_CheckBox.CheckedChanged += new System.EventHandler(this.Popolnenie_CheckBox_CheckedChanged);
            // 
            // Start_Vklad
            // 
            this.Start_Vklad.AutoSize = true;
            this.Start_Vklad.Location = new System.Drawing.Point(23, 189);
            this.Start_Vklad.Name = "Start_Vklad";
            this.Start_Vklad.Size = new System.Drawing.Size(111, 19);
            this.Start_Vklad.TabIndex = 18;
            this.Start_Vklad.Text = "Начало периода";
            // 
            // Start_Vklad_Date_Box
            // 
            this.Start_Vklad_Date_Box.Location = new System.Drawing.Point(182, 186);
            this.Start_Vklad_Date_Box.MinimumSize = new System.Drawing.Size(0, 29);
            this.Start_Vklad_Date_Box.Name = "Start_Vklad_Date_Box";
            this.Start_Vklad_Date_Box.Size = new System.Drawing.Size(146, 29);
            this.Start_Vklad_Date_Box.TabIndex = 17;
            // 
            // TotalPanel
            // 
            this.TotalPanel.Controls.Add(this.TotalLabel);
            this.TotalPanel.Controls.Add(this.metroLabel1);
            this.TotalPanel.HorizontalScrollbarBarColor = true;
            this.TotalPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.TotalPanel.HorizontalScrollbarSize = 10;
            this.TotalPanel.Location = new System.Drawing.Point(420, 3);
            this.TotalPanel.Name = "TotalPanel";
            this.TotalPanel.Size = new System.Drawing.Size(442, 27);
            this.TotalPanel.TabIndex = 16;
            this.TotalPanel.VerticalScrollbarBarColor = true;
            this.TotalPanel.VerticalScrollbarHighlightOnWheel = false;
            this.TotalPanel.VerticalScrollbarSize = 10;
            // 
            // TotalLabel
            // 
            this.TotalLabel.AutoSize = true;
            this.TotalLabel.Location = new System.Drawing.Point(212, 3);
            this.TotalLabel.Name = "TotalLabel";
            this.TotalLabel.Size = new System.Drawing.Size(37, 19);
            this.TotalLabel.TabIndex = 2;
            this.TotalLabel.Text = "Итог";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(212, 4);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(37, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Итог";
            // 
            // GetResultButton
            // 
            this.GetResultButton.Location = new System.Drawing.Point(22, 415);
            this.GetResultButton.Name = "GetResultButton";
            this.GetResultButton.Size = new System.Drawing.Size(306, 42);
            this.GetResultButton.TabIndex = 13;
            this.GetResultButton.Text = "Рассчитать";
            this.GetResultButton.UseSelectable = true;
            this.GetResultButton.Click += new System.EventHandler(this.GetResultButton_Click);
            // 
            // TotalGrid
            // 
            this.TotalGrid.AllowUserToResizeRows = false;
            this.TotalGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TotalGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TotalGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.TotalGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TotalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TotalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TotalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date_Column,
            this.Percent_Column,
            this.Deposit_Column});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TotalGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.TotalGrid.EnableHeadersVisualStyles = false;
            this.TotalGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.TotalGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TotalGrid.Location = new System.Drawing.Point(420, 32);
            this.TotalGrid.Name = "TotalGrid";
            this.TotalGrid.ReadOnly = true;
            this.TotalGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TotalGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TotalGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TotalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TotalGrid.Size = new System.Drawing.Size(442, 439);
            this.TotalGrid.TabIndex = 12;
            // 
            // Date_Column
            // 
            this.Date_Column.Frozen = true;
            this.Date_Column.HeaderText = "Дата";
            this.Date_Column.Name = "Date_Column";
            this.Date_Column.ReadOnly = true;
            this.Date_Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Percent_Column
            // 
            this.Percent_Column.Frozen = true;
            this.Percent_Column.HeaderText = "Начисленный процент";
            this.Percent_Column.Name = "Percent_Column";
            this.Percent_Column.ReadOnly = true;
            this.Percent_Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Percent_Column.Width = 200;
            // 
            // Deposit_Column
            // 
            this.Deposit_Column.Frozen = true;
            this.Deposit_Column.HeaderText = "Остаток на депозите";
            this.Deposit_Column.Name = "Deposit_Column";
            this.Deposit_Column.ReadOnly = true;
            this.Deposit_Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // d_Combo_Box
            // 
            this.d_Combo_Box.FormattingEnabled = true;
            this.d_Combo_Box.ItemHeight = 23;
            this.d_Combo_Box.Items.AddRange(new object[] {
            "Каждый месяц"});
            this.d_Combo_Box.Location = new System.Drawing.Point(182, 107);
            this.d_Combo_Box.Name = "d_Combo_Box";
            this.d_Combo_Box.Size = new System.Drawing.Size(146, 29);
            this.d_Combo_Box.TabIndex = 10;
            this.d_Combo_Box.UseSelectable = true;
            // 
            // N_Name
            // 
            this.N_Name.AutoSize = true;
            this.N_Name.Location = new System.Drawing.Point(23, 148);
            this.N_Name.Name = "N_Name";
            this.N_Name.Size = new System.Drawing.Size(107, 19);
            this.N_Name.TabIndex = 9;
            this.N_Name.Text = "Кол-во месяцев";
            // 
            // d_Name
            // 
            this.d_Name.AutoSize = true;
            this.d_Name.Location = new System.Drawing.Point(22, 111);
            this.d_Name.Name = "d_Name";
            this.d_Name.Size = new System.Drawing.Size(133, 19);
            this.d_Name.TabIndex = 7;
            this.d_Name.Text = "Период начисления";
            // 
            // P_Name
            // 
            this.P_Name.AutoSize = true;
            this.P_Name.Location = new System.Drawing.Point(22, 72);
            this.P_Name.Name = "P_Name";
            this.P_Name.Size = new System.Drawing.Size(154, 19);
            this.P_Name.TabIndex = 5;
            this.P_Name.Text = "Процентная ставка(год)";
            // 
            // P_Text_Box
            // 
            // 
            // 
            // 
            this.P_Text_Box.CustomButton.Image = null;
            this.P_Text_Box.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.P_Text_Box.CustomButton.Name = "";
            this.P_Text_Box.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.P_Text_Box.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.P_Text_Box.CustomButton.TabIndex = 1;
            this.P_Text_Box.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.P_Text_Box.CustomButton.UseSelectable = true;
            this.P_Text_Box.CustomButton.Visible = false;
            this.P_Text_Box.Lines = new string[0];
            this.P_Text_Box.Location = new System.Drawing.Point(182, 72);
            this.P_Text_Box.MaxLength = 32767;
            this.P_Text_Box.Name = "P_Text_Box";
            this.P_Text_Box.PasswordChar = '\0';
            this.P_Text_Box.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.P_Text_Box.SelectedText = "";
            this.P_Text_Box.SelectionLength = 0;
            this.P_Text_Box.SelectionStart = 0;
            this.P_Text_Box.ShortcutsEnabled = true;
            this.P_Text_Box.Size = new System.Drawing.Size(146, 23);
            this.P_Text_Box.TabIndex = 4;
            this.P_Text_Box.UseSelectable = true;
            this.P_Text_Box.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.P_Text_Box.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.P_Text_Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.P_Text_Box_KeyPress);
            this.P_Text_Box.Validating += new System.ComponentModel.CancelEventHandler(this.P_Text_Box_Validating);
            // 
            // K_Name
            // 
            this.K_Name.AutoSize = true;
            this.K_Name.Location = new System.Drawing.Point(23, 32);
            this.K_Name.Name = "K_Name";
            this.K_Name.Size = new System.Drawing.Size(153, 19);
            this.K_Name.TabIndex = 3;
            this.K_Name.Text = "Первоначальный вклад";
            // 
            // K_Text_Box
            // 
            // 
            // 
            // 
            this.K_Text_Box.CustomButton.Image = null;
            this.K_Text_Box.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.K_Text_Box.CustomButton.Name = "";
            this.K_Text_Box.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.K_Text_Box.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.K_Text_Box.CustomButton.TabIndex = 1;
            this.K_Text_Box.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.K_Text_Box.CustomButton.UseSelectable = true;
            this.K_Text_Box.CustomButton.Visible = false;
            this.K_Text_Box.Lines = new string[0];
            this.K_Text_Box.Location = new System.Drawing.Point(182, 32);
            this.K_Text_Box.MaxLength = 32767;
            this.K_Text_Box.Name = "K_Text_Box";
            this.K_Text_Box.PasswordChar = '\0';
            this.K_Text_Box.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.K_Text_Box.SelectedText = "";
            this.K_Text_Box.SelectionLength = 0;
            this.K_Text_Box.SelectionStart = 0;
            this.K_Text_Box.ShortcutsEnabled = true;
            this.K_Text_Box.Size = new System.Drawing.Size(146, 23);
            this.K_Text_Box.TabIndex = 2;
            this.K_Text_Box.UseSelectable = true;
            this.K_Text_Box.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.K_Text_Box.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.K_Text_Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.K_Text_Box_KeyPress);
            this.K_Text_Box.Validating += new System.ComponentModel.CancelEventHandler(this.K_Text_Box_Validating);
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.label1);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(907, 508);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Справка";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(247, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(413, 52);
            this.label1.TabIndex = 2;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 550);
            this.Controls.Add(this.metroPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VkladCalculator";
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.MainTabControl.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.TotalPanel.ResumeLayout(false);
            this.TotalPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).EndInit();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTabControl MainTabControl;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTextBox K_Text_Box;
        private MetroFramework.Controls.MetroButton GetResultButton;
        private MetroFramework.Controls.MetroGrid TotalGrid;
        private MetroFramework.Controls.MetroComboBox d_Combo_Box;
        private MetroFramework.Controls.MetroLabel N_Name;
        private MetroFramework.Controls.MetroLabel d_Name;
        private MetroFramework.Controls.MetroLabel P_Name;
        private MetroFramework.Controls.MetroTextBox P_Text_Box;
        private MetroFramework.Controls.MetroLabel K_Name;
        private MetroFramework.Controls.MetroTextBox Snyatie_TextBox;
        private MetroFramework.Controls.MetroLabel Snyatie_Name;
        private MetroFramework.Controls.MetroDateTime Snyatie_DateBox;
        private MetroFramework.Controls.MetroCheckBox Snyatie_CheckBox;
        private MetroFramework.Controls.MetroTextBox Popolnenie_TextBox;
        private MetroFramework.Controls.MetroLabel Popolnenie_Name;
        private MetroFramework.Controls.MetroDateTime Popolnenie_DateBox;
        private MetroFramework.Controls.MetroCheckBox Popolnenie_CheckBox;
        private MetroFramework.Controls.MetroLabel Start_Vklad;
        private MetroFramework.Controls.MetroDateTime Start_Vklad_Date_Box;
        private MetroFramework.Controls.MetroPanel TotalPanel;
        private MetroFramework.Controls.MetroLabel TotalLabel;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn Percent_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn Deposit_Column;
        private MetroFramework.Controls.MetroTextBox N_Text_Box;
        private System.Windows.Forms.Label label1;
    }
}

