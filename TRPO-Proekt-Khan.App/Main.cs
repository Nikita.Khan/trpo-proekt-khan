﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using TRPO_Proekt_Khan;

namespace TRPO_Proekt_Khan.App
{
    public partial class Main : Form
    {
       
        public Main()
        {
            InitializeComponent();
            Popolnenie_DateBox.Enabled = false;
            Popolnenie_TextBox.Enabled = false;
            Snyatie_DateBox.Enabled = false;
            Snyatie_TextBox.Enabled = false;

        }



        private void Popolnenie_CheckBox_CheckedChanged(object sender, EventArgs e)
        {//Включаем или выключаем возможность пользования объектами в зависимости от чекбокса
            if (Popolnenie_DateBox.Enabled)
            {
                Popolnenie_DateBox.Enabled = false;
                Popolnenie_TextBox.Enabled = false;
            }
            else
            {
                Popolnenie_DateBox.Enabled = true;
                Popolnenie_TextBox.Enabled = true;
            }
        }

        private void Snyatie_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Snyatie_DateBox.Enabled)
            {
                Snyatie_DateBox.Enabled = false;
                Snyatie_TextBox.Enabled = false;
            }
            else
            {
                Snyatie_DateBox.Enabled = true;
                Snyatie_TextBox.Enabled = true;
            }
        }

        private void GetResultButton_Click(object sender, EventArgs e)
        {
            string EmptyTotal = "Заполните поля: ";
            bool flag = false;
            if (K_Text_Box.Text == String.Empty) //если пусто
            {
                EmptyTotal += "| " + K_Name.Text + " |"; //в итоговую строку добавляем имя поля
                flag = true; //будем знать что были пустные поля
            }
            if (P_Text_Box.Text == String.Empty)
            {
                EmptyTotal += "| " + P_Name.Text + " |";
                flag = true;
            }
            if (d_Combo_Box.Text == String.Empty)
            {
                EmptyTotal += "| " + d_Name.Text + " |";
                flag = true;
            }
            if (N_Text_Box.Text == String.Empty)
            {
                EmptyTotal += "| " + N_Name.Text + " |";
                flag = true;
            }
            if (Popolnenie_CheckBox.Checked) //если есть пополнение
                if (Popolnenie_TextBox.Text == String.Empty)
                {
                    EmptyTotal += "| Сумма пополнения |";
                    flag = true;
                }
            if (Snyatie_CheckBox.Checked) //если есть снятие
                if (Snyatie_TextBox.Text == String.Empty)
                {
                    EmptyTotal += "| Сумма Снятия |";
                    flag = true;
                }
                else if ((K_Text_Box.Text != String.Empty) && (Convert.ToInt32(Snyatie_TextBox.Text) > Convert.ToInt32(K_Text_Box.Text))) //если строка с первоначальным вкладом не пустная, то проверяем сумму снятия на преувеличение
                    MessageBox.Show(this, "Сумма снятия не должна превышать первоначальный вклад");
       
            if (flag) //если хоть одно поле пустое мы об этом сообщим и скажем какие именно
            {
                MessageBox.Show(this, EmptyTotal);
                return;//выходим
            }

            //собираем все нужные нам данные
            int K, P, N;
            double S, SP = 0;
            K = P = N = 0;
            int D = 365;
            DateTime Start = Start_Vklad_Date_Box.Value.Date; //Дата начала вклада
            DateTime End = Start.AddMonths(N); //Дата конца вклада (добавили кол-во месяцев,которые указали)
            DateTime PopolnenieDate = Start; //Дата пополнения
            DateTime SnyatieDate = Start; //Дата снятия
            DateTime ActualDate = Start; //при рассчетах будет указывать начисления за текущий период
       

            try //берем данные
            {
                K = Convert.ToInt32(K_Text_Box.Text); //капитал
                P = Convert.ToInt32(P_Text_Box.Text); //процентная ставка
                N = Convert.ToInt32(N_Text_Box.Text); //кол-во месяцев
            }
            catch (Exception er)
            {
                MessageBox.Show(this, "ошибка при взятии данных");
            }

            if ((Popolnenie_CheckBox.Checked || Snyatie_CheckBox.Checked) && (Popolnenie_DateBox.Value.Date >= Start) &&
                (Popolnenie_DateBox.Value.Date <= End) && (Snyatie_DateBox.Value.Date >= Start) && (Snyatie_DateBox.Value.Date <= End)) // если есть пополнение или снятие, то их даты должны попадать в период взятия вклада
            {
                MessageBox.Show(this, "Даты Пополнения/Снятия должны попадать в период взятия вклада!");
                return;
            }
            if (Popolnenie_CheckBox.Checked || Snyatie_CheckBox.Checked) //Если мы прошли проверку данных в "пополнение" и "снятие", то присваиваем нужным переменным
            {
                PopolnenieDate = Popolnenie_DateBox.Value.Date; 
                SnyatieDate = Snyatie_DateBox.Value.Date; 
               
            }
            var Calculator = new VkladCalculator();
            TotalGrid.Rows.Clear();
            TotalGrid.Refresh();
            TotalGrid.Rows.Add(Start.ToShortDateString(), Math.Round(SP, 2), K);
            for(int i=0;i<N;i++)
            {
                

                if (Popolnenie_CheckBox.Checked && PopolnenieDate.Month == ActualDate.Month) //если есть пополнение и оно в этом месяце
                {
        
                    int _d = Convert.ToInt32(PopolnenieDate.Day) - Convert.ToInt32(Start.Day); //Сколько дней до пополнения процентов
                    int d = DateTime.DaysInMonth(ActualDate.Year, ActualDate.Month) - _d; //сколько уже после пополнения и все это в одном месяце (тут мы вычислили сколько дней в месяце и отняли те дни что до пополнения)
                    S = Calculator.GetPercents(K + Convert.ToInt32(Popolnenie_TextBox.Text), P, d, D, 1) + Calculator.GetPercents(K, P, _d, D, 1); //сложили проценты с дней до пополнения и после в одном месяце
                    SP += S; //добавили в общую сумму начисленных процентов
                    K = K + Convert.ToInt32(Popolnenie_TextBox.Text); //текущий депозит
                    TotalGrid.Rows.Add(ActualDate.AddMonths(1).ToShortDateString(), Math.Round(S, 2), K);
                    
                }
                else if (Snyatie_CheckBox.Checked && SnyatieDate.Month == ActualDate.Month) //тоже самое если снятие
                {
                    
                    int _d = Convert.ToInt32(PopolnenieDate.Day) - Convert.ToInt32(Start.Day); //Сколько дней до снятия процентов
                    int d = DateTime.DaysInMonth(ActualDate.Year, ActualDate.Month) - _d; //сколько уже после снятия и все это в одном месяце (тут мы вычислили сколько дней в месяце и отняли те дни что до снятия)
                    S = Calculator.GetPercents(K - Convert.ToInt32(Popolnenie_TextBox.Text), P, d, D, 1) + Calculator.GetPercents(K, P, _d, D, 1); //сложили проценты с дней до снятия и после в одном месяце
                    SP += S; //добавили в общую сумму начисленных процентов
                    K = K - Convert.ToInt32(Popolnenie_TextBox.Text); //текущий депозит
                    TotalGrid.Rows.Add(ActualDate.AddMonths(1).ToShortDateString(), Math.Round(S, 2), K);
                    
                }
                //else //если не пополнение и не снятие, то просто как обычно считаем проценты
               // {
                   S = Calculator.GetPercents(K, P, DateTime.DaysInMonth(ActualDate.Year, ActualDate.Month), D, 1);
                SP += S;
                TotalGrid.Rows.Add(ActualDate.AddMonths(1).ToShortDateString(), Math.Round(S, 2), K); //вывод в итоги
               // }
                ActualDate = ActualDate.AddMonths(1);
            }
            TotalGrid.Rows.Add("Итого", Math.Round(SP,2), K); //вывод в итоги

        }

        private void K_Text_Box_Validating(object sender, CancelEventArgs e) //Проверка введеных данных
        {
           
        }

        private void P_Text_Box_Validating(object sender, CancelEventArgs e)
        {
          
        }

        private void K_Text_Box_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar)&& !Char.IsControl(e.KeyChar)) //если это не символ и не управляющая кнопка ввод разрешен
                e.Handled = true;
        }

        private void P_Text_Box_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar)) //если это не символ и не управляющая кнопка ввод разрешен
                e.Handled = true;
        }

        private void N_Text_Box_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar)) //если это не символ и не управляющая кнопка ввод разрешен
                e.Handled = true;
        }

        private void Popolnenie_TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar)) //если это не символ и не управляющая кнопка ввод разрешен
                e.Handled = true;
        }

        private void Snyatie_TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar)) //если это не символ и не управляющая кнопка ввод разрешен
                e.Handled = true;
        }

        private void Snyatie_TextBox_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void N_Text_Box_Validating(object sender, CancelEventArgs e)
        {
        }

        private void N_Text_Box_TextChanged(object sender, EventArgs e)
        {
            if (N_Text_Box.Text!="" && Convert.ToInt32(N_Text_Box.Text) > 600)//если в кол-во месяцев больше 600(30лет)
                MessageBox.Show(this, "Слишком большой период!");
        }
    }
}
